import * as _ from 'lodash'

export interface IRedisEvents {
    redisChannels: {newMessage: string, messageDelivered: string}
}
const commonConfig = {
    redisChannels: {
        newMessage: 'NEW_MESSAGE',
        messageDelivered: 'MESSAGE_DELIVERED'
    }
};

const redisEvents: IRedisEvents = commonConfig;

export default redisEvents
