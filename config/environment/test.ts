// Test specific configuration
// ===========================
export default {
  allowedOrigins: [
    '*'
  ],
  logger: {
    ipAddress: '192.168.0.15',
    serviceName: 'chat-service',
    port: 24224
  },

  kafka: {
    clientId: 'ChatService',
    brokers: ['192.168.0.15:9092'],
    groupId: 'ChatService'
  }
};
