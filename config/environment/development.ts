'use strict';

// Development specific configuration
// ==================================
export default {
  allowedOrigins: [
    'http://localhost:9000',
  ],

  logger: {
    ipAddress: '172.104.243.53',
    serviceName: 'chat-service',
    port: 24224
  },

  kafka: {
    clientId: 'ChatService',
    brokers: ['172.104.243.53:9092'],
    groupId: 'ChatService'
  }
};
