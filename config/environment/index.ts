import * as _ from 'lodash'

import developmentConfig from './development'
import productionConfig from './production'
import testConfig from './test'

export interface IConfig {
  allowedOrigins?: string[],
  appName?: string
  env?: string
  ip?: string
  port?: number
  redisPort?: number,
  redisUrl?: string,
  logger?: {
    ipAddress: string,
    port: number,
    serviceName: string
  },
  kafka?: {
    clientId: string,
    brokers: [],
    groupId: string
  }
  secrets?: {
    session?: string
  },

  adminKey: string
}

// All configurations will extend these options
// ============================================
const commonConfig = {
  appName: process.env.APP_NAME || `hunteed-${process.env.NODE_ENV}`,
  env: process.env.NODE_ENV,
  redisPort: Number(process.env.REDIS_PORT) || 6379,
  redisUrl: process.env.REDIS_URL || '192.168.0.10',
  // Server port
  port: Number(process.env.PORT) || 9000,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: process.env.SECRET_SESSION || 'superSecr3t'
  },

  adminKey: '28aIGN[AA~1F?4_vBH78y9RxEN<6NJ~""VXeVi5pFVPr`0W,E(36@=yFP5'
};

// Export the config object based on the NODE_ENV
// ==============================================
const config: IConfig = commonConfig;

if (commonConfig.env === 'development') {
  _.merge(config, developmentConfig)
} else if (commonConfig.env === 'test') {
  _.merge(config, testConfig)
} else if (commonConfig.env === 'production') {
  _.merge(config, productionConfig)
} else {
  throw new Error('Please set an environment')
}

export default config
