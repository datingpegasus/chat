import {
    IComment,
    IFeedBeforeSave,
    IFeedLike,
    IMessage,
    IUserConnectedToConversation,
    IUserIsTyping,
    ICommentBeforeSave
} from 'types';
import {ISocketService} from '../models';
import * as socketIo from 'socket.io'
import {IUserService} from '../User/UserService';

export default class SocketListeners {
    private readonly socket: socketIo.Socket;
    private readonly socketService: ISocketService;
    private readonly userService: IUserService;
    public constructor(socket: socketIo.Socket, socketService: ISocketService, userService: IUserService) {
        this.socket = socket;
        this.socketService = socketService;
        this.userService = userService;
        this.listenForNewMessage();
        this.listenForDisconnect();
        this.listenForUserTyping();
        this.listenForUserConnectedToConversation();
        this.listenForUserDisconnectedFromConversation();
        this.messageDelivered();
        this.feeds();
        this.newFeed();
        this.newFeedLike();
        this.userChangedRoom();
        this.newComment();
        this.comments();
    }

    private listenForNewMessage(): void {
        this.socket.on('message', (message: string) => {
            this.socketService.newMessage(JSON.parse(message))
        });
    }

    private listenForDisconnect(): void {
        this.socket.on('disconnect', () => {
            this.socketService.userDisconnected(this.socket);
        })
    }

    private listenForUserTyping(): void {
        this.socket.on('user-is-typing', (userIsTypingData: IUserIsTyping) => {
            this.socketService.userIsTyping(userIsTypingData);
        })
    }

    private listenForUserConnectedToConversation(): void {
        this.socket.on('user-connected-to-conversation', async (userConnectedToConversation: IUserConnectedToConversation) => {
            await this.socketService.userConnectedToConversation(userConnectedToConversation);
        })
    }

    private listenForUserDisconnectedFromConversation(): void {
        this.socket.on('user-disconnected-from-conversation', (user: { userId: string }) => {
            this.socketService.userDisconnectedFromConversation(user.userId)
        })
    }

    private messageDelivered(): void {
        this.socket.on('message-delivered', (message: IMessage) => {
            this.socketService.messageDelivered(message);
        });
    }

    private feeds(): void {
        this.socket.on('feeds', (userId: string) => {
            this.socketService.publishNewFeeds(userId);
        });
    }

    private newFeed(): void {
        this.socket.on('new-feed', (feed: IFeedBeforeSave) => {
            this.socketService.newFeed(feed);
        });
    }

    private newFeedLike(): void {
        this.socket.on('new-feed-like', (feedLike: IFeedLike) => {
            this.socketService.newFeedLike(feedLike);
        });
    }

    private userChangedRoom(): void {
        this.socket.on('user-changed-screen', (data: any) => {
            this.userService.userChangedScreen(data);
        });
    }

    private newComment(): void {
        this.socket.on('new-comment', (comment: ICommentBeforeSave) => {
            this.socketService.newComment(comment);
        })
    }

    private comments(): void {
        this.socket.on('comments', (data: any) => {
            this.socketService.comments(data.feedId, data.userId);
        });
    }
}
