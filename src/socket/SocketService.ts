import {
    IKafkaProducer,
    ISocketService,
    IValidator,
    IUserSocketRepository,
    IUserSocketEmit,
    IRedis,
    RedisKeys
} from '../models';
import {
    IMessage,
    IInitConversationMessages,
    IUserConversations,
    IUserConnectedToConversation,
    IUserIsTyping,
    IAllFeeds, IFeed, IFeedBeforeSave, IFeedLike, IComment, ICommentBeforeSave, ICommentsForFeed,
    IStory, IStories
} from 'types'
import * as socketIo from 'socket.io';
import Logger from '../services/Logger';
import {IUserRoomStorage, Rooms} from '../User/UserRoomStorage';

export default class SocketService implements ISocketService {
    private readonly validator: IValidator;
    private readonly userSocketRepository: IUserSocketRepository;
    private readonly userSocketEmit: IUserSocketEmit;
    private readonly kafkaProducer: IKafkaProducer;
    private readonly redis: IRedis;
    private readonly userRoomStorage: IUserRoomStorage;
    constructor(validator: IValidator,
                userSocketRepository: IUserSocketRepository,
                kafkaProducer: IKafkaProducer,
                userSocketEmit: IUserSocketEmit,
                redis: IRedis,
                userRoomStorage: IUserRoomStorage) {
        this.validator = validator;
        this.userSocketRepository = userSocketRepository;
        this.userSocketEmit = userSocketEmit;
        this.kafkaProducer = kafkaProducer;
        this.redis = redis;
        this.userRoomStorage = userRoomStorage;

    }

    public async newMessage(message: IMessage): Promise<boolean> {
        if (this.validator.isMessageValid(message)) {
            await this.kafkaProducer.dispatchNewMessage(message);
            return true;
        } else {
            this.userSocketEmit.emitBadMessageFormat(
                this.userSocketRepository.socketForUserId(message.senderId),
                message.id
            );
            return false;
        }
    }

    public userIsTyping(userTypingData: IUserIsTyping): void {
        if (this.validator.isTypingDataValid((userTypingData))) {
            this.userSocketEmit.emitUserIsTyping(
                this.userSocketRepository.socketForUserId(userTypingData.receiverId), userTypingData);
        }
    }

    public userDisconnected(socket: socketIo.Socket): void {
        try {
            const userIdFromSocket = this.userSocketRepository.userIdFromSocket(socket);
            this.userSocketRepository.userDisconnected(socket);
            this.redis.remove(userIdFromSocket);
            this.userDisconnectedFromConversation(userIdFromSocket);
            this.userRoomStorage.removeUserFromAllRooms(userIdFromSocket);
            Logger.userDisconnected(userIdFromSocket);
        } catch (e) {
            Logger.error(e);
        }
    }

    public messageDelivered(message: IMessage): void {
        //TODO Handle Edge Cases
        this.kafkaProducer.dispatchNewMessageDelivered(message);
    }

    //TODO Test I
    public async sendNewMessageToUser(message: IMessage): Promise<boolean> {
        const userSocket = this.userSocketRepository.socketForUserId(message.receiverId);
        if (userSocket === undefined) return false;
        const userActiveConversation = await this.redis.hGet(RedisKeys.activeUserConversations, message.receiverId);
        if (userActiveConversation === message.conversationId) {
            this.userSocketEmit.emitMessageToUser(userSocket, message);
            return true;
        }

        return this.kafkaProducer.dispatchUserNotInConversation(message);
    }

    public emitFailedToSaveMessage(message: any): boolean {
        const userSocket = this.userSocketRepository.socketForUserId(message.message.senderId);
        if (userSocket === undefined) return false;
        this.userSocketEmit.emitFailedToSaveMessageInDatabase(userSocket, message.message);
        return true;
    }

    public emitConfirmMessageDelivered(message: IMessage): boolean {
        const userSocket = this.userSocketRepository.socketForUserId(message.senderId);
        if (userSocket === undefined) return false;
        this.userSocketEmit.emitMessageDelivered(userSocket, message);
        return true;
    }

    public userStatusChange(): boolean {
        return true;
    }

    public getUserSocketEmit(): IUserSocketEmit {
        return this.userSocketEmit;
    }

    public getUserSocketRepository(): IUserSocketRepository {
        return this.userSocketRepository;
    }

    public pushUserConnectedToSocket(userId: string): void {
        this.kafkaProducer.dispatchUserConnectedToSocket(userId);
    }

    public sendUserConversationsToUser(conversations: IUserConversations): boolean {
        const userSocket = this.userSocketRepository.socketForUserId(conversations.userId);
        if (userSocket !== undefined) {
            this.userSocketEmit.emitUserConversations(userSocket, conversations.conversations);
            return true;
        }
        return false;
    }

    public async userConnectedToConversation(userConnectedToConversation: IUserConnectedToConversation): Promise<boolean> {
        try {
            const redisConversation: string = await this.redis.hGet(RedisKeys.activeUserConversations,
                userConnectedToConversation.conversationId);

            if (redisConversation === userConnectedToConversation.conversationId) {
                return await this.kafkaProducer.userConnectedToConversation(userConnectedToConversation);
            }

            await this.redis.hSet(RedisKeys.activeUserConversations,
                userConnectedToConversation.userId, userConnectedToConversation.conversationId);
            return await this.kafkaProducer.userConnectedToConversation(userConnectedToConversation);
        } catch (e) {
            return false;
        }
    }

    public async userDisconnectedFromConversation(userId: string): Promise<boolean> {
        try {
            return await this.redis.hRemove(RedisKeys.activeUserConversations, userId)
        } catch (e) {
            return false;
        }
    }

    public sendUserConversationMessages(conversationObject: IInitConversationMessages): boolean {
        const userSocket = this.userSocketRepository.socketForUserId(conversationObject.userId);
        if (userSocket === undefined) return true; //commit messages user disconnected
        this.userSocketEmit.emitUserConversationMessages(userSocket, conversationObject.messages);
        return true;
    }
    public newFeed(feed: IFeedBeforeSave): void {
        this.kafkaProducer.dispatchNewFeed(feed);
    }

    public publishNewFeeds(userId: string): void {
        this.kafkaProducer.dispatchFetchFeeds(userId);
    }

    public feedsFetchingComplete(feeds: IAllFeeds): boolean {
        const userSocket = this.userSocketRepository.socketForUserId(feeds.userId);
        if (userSocket === undefined) return true;
        this.userSocketEmit.emitFeedsToUser(userSocket, feeds.feeds);
        return true;
    }

    public newFeedSaved(feeds: IFeed): boolean {
        const clients: socketIo.Socket[] = this.userSocketRepository.getAllClients();
        for (const key in clients) {
            if (clients.hasOwnProperty(key)) {
                this.userSocketEmit.emitNewFeedToUser(clients[key], feeds);
            }
        }
        return true;
    }

    public newFeedLike(feedLike: IFeedLike): void {
        this.kafkaProducer.dispatchNewFeedLike(feedLike);
    }

    public newFeedLikeSaved(feedLike: IFeedLike): boolean {
        const clients: socketIo.Socket[] = this.userSocketRepository.getAllClients();
        for (const key in clients) {
            if (clients.hasOwnProperty(key)) {
                this.userSocketEmit.emitNewFeedLike(clients[key], feedLike);
            }
        }
        return true;
    }

    public newComment(comment: ICommentBeforeSave): void {
        this.kafkaProducer.dispatchNewComment(comment);
    }

    public newCommentSaved(comment: IComment): boolean {
        const users = this.userRoomStorage.getUsersFromRoom(Rooms.FeedScreen);
        console.log(users);
        for (const userId of users) {
            const userSocket = this.userSocketRepository.socketForUserId(userId);
            console.log('emit for user id ', userId);
            if (userSocket !== undefined) this.userSocketEmit.emitNewComment(userSocket, comment);
        }
        return true;
    }

    public comments(feedId: number, userId: number): void {
        this.kafkaProducer.dispatchCommentsForFeedId(feedId, userId);
    }

    public commentsFetched(data: ICommentsForFeed): boolean {
        const userSocket = this.userSocketRepository.socketForUserId(data.userId);
        if (userSocket === undefined) return true;
        this.userSocketEmit.emitCommentsFetched(userSocket, data.comments);
        return true;
    }

    public newStorySaved(story: IStory): boolean {
        const users = this.userRoomStorage.getUsersFromRoom(Rooms.FeedScreen);
        for (const userId of users) {
            const userSocket = this.userSocketRepository.socketForUserId(userId);
            if (userSocket !== undefined) this.userSocketEmit.emitNewStory(userSocket, story);
        }
        return true;
    }

    public storiesFetched(data: IStories): boolean {
        const userSocket = this.userSocketRepository.socketForUserId(data.userId);
        if (userSocket === undefined) return true;
        this.userSocketEmit.emitStoriesFetched(userSocket, data.stories);
        return true;
    }
}
