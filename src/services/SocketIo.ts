import * as socketIo from 'socket.io';
import * as express from 'express'
import {IUserSocketRepository, ISocketService} from '../models';
import SocketListeners from '../socket/SocketListeners';
import Logger from './Logger';
import {IUserService} from '../User/UserService';
import UserSocketListeners from '../User/UserSocketListeners';

export default class SocketIo {
    private io: socketIo.Server;
    private socket: socketIo.Socket;
    private readonly server: express.Application;
    private readonly userSocketRepository: IUserSocketRepository;
    private readonly socketService: ISocketService;
    private readonly userService: IUserService;
    private socketListener: object;
    public constructor(server: any, userSocketRepository: IUserSocketRepository, socketService: ISocketService, userService: IUserService) {
        this.server = server;
        this.userSocketRepository = userSocketRepository;
        this.socketService = socketService;
        this.userService = userService;
        this.socketListener = {};
        this.sockets();
    }

    private sockets(): void {
        this.io = socketIo(this.server)
    }

    public async connect(): Promise<socketIo.Socket> {
        return new Promise<socketIo.Socket>((resolve, reject) => {
            try {
                this.io.on('connection', (socket: socketIo.Socket) => {
                    if (this.userSocketRepository.isUserAlreadyConnected(socket)) {
                        console.log('wtf it shgould be there');
                        return resolve(socket);
                    }
                    console.log('new connection');
                    if (this.userSocketRepository.userConnected(socket)) {
                        new SocketListeners(socket, this.socketService, this.userService);
                        new UserSocketListeners(socket, this.userService);
                        const userId = this.userSocketRepository.userIdFromSocket(socket);
                        this.socketService.pushUserConnectedToSocket(userId);
                        Logger.userConnected(userId);
                    }
                    resolve(socket);
                });
                return resolve(this.socket);
            } catch (e) {
                Logger.error(e);
                reject(e);
            }
        });

    }
}
