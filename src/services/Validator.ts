import {IMessage, IUserIsTyping} from 'types';
import {IValidator} from '../models'
export default class Validator implements IValidator {
    //TODO This needs to be replaced with different validator
    public isMessageValid(message: IMessage): boolean {
        return true;
    }

    public isTypingDataValid(userTypingData: IUserIsTyping): boolean {
        return true;
    }
}
