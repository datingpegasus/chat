import * as socketIo from 'socket.io';
import {
    IMessage,
    IInitConversationMessages,
    IUserConversations,
    IUserConnectedToConversation,
    IUserIsTyping,
    IStorageMessage,
    IAllFeeds, IFeed, IFeedBeforeSave, IFeedLike, IComment, ICommentBeforeSave, ICommentsForFeed
} from 'types'

export interface ISocketService {
    newMessage(message: IMessage): Promise<boolean>;
    userDisconnected(socket: socketIo.Socket): void;
    userIsTyping(userTypingData: IUserIsTyping): void;
    sendNewMessageToUser(message: IMessage): Promise<boolean>;
    emitFailedToSaveMessage(message: any): boolean;
    messageDelivered(message: IMessage): void;
    getUserSocketEmit(): IUserSocketEmit;
    getUserSocketRepository(): IUserSocketRepository;
    userStatusChange(): boolean;
    emitConfirmMessageDelivered(message: IMessage): boolean;
    pushUserConnectedToSocket(userId: string): void;
    sendUserConversationsToUser(conversations: IUserConversations): boolean;
    sendUserConversationMessages(conversationObject: IInitConversationMessages): boolean;
    userConnectedToConversation(userConnectedToConversation: IUserConnectedToConversation): Promise<boolean>;
    userDisconnectedFromConversation(userId: string): Promise<boolean>
    publishNewFeeds(userId: string): void;
    feedsFetchingComplete(feeds: IAllFeeds): boolean
    newFeed(feed: IFeedBeforeSave): void;
    newFeedSaved(feeds: IFeed): boolean;
    newFeedLike(feedLike: IFeedLike): void;
    newFeedLikeSaved(feedLike: IFeedLike): boolean;
    newComment(comment: ICommentBeforeSave): void;
    newCommentSaved(comment: IComment): boolean;
    comments(feedId: number, userId: number): void;
    commentsFetched(data: ICommentsForFeed): boolean;
}

export interface IUserSocketEmit {
    emitBadMessageFormat(socket: socketIo.Socket, message_hash: string): void;
    emitMessageToUser(socket: SocketIO.Socket, message: IMessage): void
    emitUserIsTyping(socket: socketIo.Socket, userIsTypingData: IUserIsTyping): void;
    emitMessageDelivered(socket: SocketIO.Socket, message: IMessage): void;
    emitFailedToSaveMessageInDatabase(socket: SocketIO.Socket, message: IMessage): void;
    emitUserConversations(socket: socketIo.Socket, conversations: IMessage[]): void;
    emitUserConversationMessages(socket: socketIo.Socket, messages: IStorageMessage[]): void;
    emitFeedsToUser(socket: socketIo.Socket, feeds: IFeed[]): void;
    emitNewFeedToUser(socket: socketIo.Socket, feed: IFeed): void;
    emitNewFeedLike(socket: socketIo.Socket, feedLike: IFeedLike): void;
    emitNewComment(socket: socketIo.Socket, comment: IComment): void;
    emitCommentsFetched(socket: socketIo.Socket, comments: IComment[]): void;
    emitNewStory(socket: socketIo.Socket, story: IStory): void;
    emitStoriesFetched(socket: socketIo.Socket, stories: IStory[]): void;
}

export interface IUserSocketRepository {
    isUserAlreadyConnected(socket: socketIo.Socket): boolean;
    userConnected(socket: socketIo.Socket): boolean;
    userDisconnected(socket: socketIo.Socket): void;
    socketForUserId(userId: string | number): socketIo.Socket | undefined;
    isAdmin(socket: socketIo.Socket): boolean;
    userIdFromSocket(socket: socketIo.Socket): string;
    getAllClients(): []
}

export interface IUserSocketStorage {
    setSocket(userId: string, socket: socketIo.Socket): void;
    socketForUser(userId: string): socketIo.Socket | undefined;
    removeUser(userId: string): void;
    getAllClients(): [];
}
