import * as redis from 'redis'
export interface IRedis {
    connectToRedis(): Promise<redis.RedisClient>
    subscribe(client: redis.RedisClient): Promise<redis.RedisClient>;
    exists(key: string): Promise<boolean>;
    set(key: string, value: string): Promise<boolean>;
    get(key: string): Promise<string>;
    hGetAll(key: string): Promise<object>
    hSet(key: string, hKey: string, value: string): Promise<boolean>
    remove(key: string): Promise<boolean>
    hGet(key: string, indexKey: string): Promise<string>
    hRemove(key: string, indexKey: string): Promise<boolean>;
}

export enum RedisKeys {
    activeUserConversations = 'activeUserConversations',
    onlineUsers = 'online-users'
}
