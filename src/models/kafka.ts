import {IComment, IFeedBeforeSave, IFeedLike, IMessage, IUserConnectedToConversation, ICommentBeforeSave} from 'types';
import * as kafka from 'kafkajs';

export interface IKafkaProducer {
    dispatchNewMessage(message: IMessage): Promise<boolean>
    dispatchNewMessageDelivered(message: IMessage): Promise<boolean>
    dispatchUserConnectedToSocket(userId: string): Promise<boolean>
    userConnectedToConversation(userConnectedToConversation: IUserConnectedToConversation): Promise<boolean>;
    dispatchUserNotInConversation(message: IMessage): Promise<boolean>;
    dispatchFetchFeeds(userId: string): Promise<boolean>;
    dispatchNewFeed(feed: IFeedBeforeSave): Promise<boolean>;
    dispatchNewFeedLike(feedLike: IFeedLike): Promise<boolean>;
    dispatchNewComment(comment: ICommentBeforeSave): Promise<boolean>;
    dispatchNewImageUploaded(data: any): Promise<boolean>;
    dispatchNewStoryAdded(data: any): Promise<boolean>;
    dispatchCommentsForFeedId(feedId: number, userId: number): Promise<boolean>;
}

export interface IKafkaConsumer {
    processKafkaTopic(topic: string, kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void>;
    subscribeToTopics(): void;
}
