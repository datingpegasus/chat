import {IMessage} from './message';
import {IUserIsTyping} from './user';

export interface IValidator {
    isMessageValid(message: IMessage): boolean;
    isTypingDataValid(userTypingData: IUserIsTyping): boolean;
}
