export interface IUserRoomStorage {
    getUsersFromRoom(room: string): [];
    setUserInRoom(room: string, userId: string): void;
    removeUserFromRoom(room: string, userId: string): void;
    removeUserFromAllRooms(userId: string): void;
}
export enum Rooms {
    ChatScreen = 'ChatScreen',
    FeedScreen = 'FeedScreen'
}
export default class UserRoomStorage implements IUserRoomStorage {
    private readonly rooms: {};
    public constructor() {
        this.rooms = {};
        //@ts-ignore
        this.rooms[Rooms.ChatScreen] = [];
        //@ts-ignore
        this.rooms[Rooms.FeedScreen] = [];
    }
    public getUsersFromRoom(room: Rooms): [] {
        //@ts-ignore
        if (this.rooms[room] === undefined) return [];
        //@ts-ignore
        return this.rooms[room];
    }

    public removeUserFromRoom(room: any, userId: string): void {
        //@ts-ignore
        if (this.rooms[room] === undefined) return;
        //@ts-ignore
        this.rooms[room] = this.rooms[room].filter((value: any) => {
            return userId === value
        })
    }

    public removeUserFromAllRooms(userId: string): void {
        for (const userRoom in this.rooms) {
            if (this.rooms.hasOwnProperty(userRoom)) {
                this.removeUserFromRoom(userRoom, userId)
            }
        }
    }

    public setUserInRoom(room: string, userId: string): void {
        //@ts-ignore
        if (this.rooms[room] === undefined) return;
        if (this.rooms[room].includes(userId) === false) {
            //@ts-ignore
            this.rooms[room].push(userId);
        }
        console.log(this.rooms);
    }
}
