import {IUserService} from './UserService';
import * as socketIo from 'socket.io'
export default class UserSocketListeners {
    private readonly userService: IUserService;
    private readonly socket: socketIo.Socket;
    constructor(socket: socketIo.Socket, userService: IUserService) {
        this.userService = userService;
        this.socket = socket;
        this.listenForNewImage();
        this.listenForNewStory();
    }

    private listenForNewImage(): void {
        this.socket.on('new-image-upload', async (data: any) => {
           await this.userService.newImageUploaded(data);
        });
    }

    private listenForNewStory(): void {
        this.socket.on('new-story-added', async (data: any) => {
           await this.userService.newStoryAdded(data);
        });
    }
}
