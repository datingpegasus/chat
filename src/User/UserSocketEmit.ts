import * as socketIo from 'socket.io';
import {IUserIsTyping, IMessage, IStorageMessage, IFeed, IFeedLike, IComment, IStory} from 'types';
import {IUserSocketEmit} from '../models'
export default class UserSocketEmit implements IUserSocketEmit {

    private static instance: UserSocketEmit;

    public static factory(): UserSocketEmit {
        if (!UserSocketEmit.instance) {
            UserSocketEmit.instance = new UserSocketEmit()
        }
        return UserSocketEmit.instance;
    }

    public emitBadMessageFormat(socket: SocketIO.Socket, message_hash: string): void {
        socket.emit('bad-message', {message_hash})
    }

    public emitMessageToUser(socket: SocketIO.Socket, message: IMessage): void {
        socket.emit('new-message', message)
    }

    public emitUserIsTyping(socket: SocketIO.Socket, userIsTypingData: IUserIsTyping): void {
        socket.emit('user-is-typing', userIsTypingData);
    }

    public emitMessageDelivered(socket: SocketIO.Socket, message: IMessage): void {
        socket.emit('message-delivered', message)
    }

    public emitFailedToSaveMessageInDatabase(socket: SocketIO.Socket, message: IMessage): void {
        socket.emit('message-not-saved', message);
    }

    public emitUserConversations(socket: socketIo.Socket, conversations: IMessage[]): void {
        socket.emit('user-conversations', conversations)
    }

    public emitUserConversationMessages(socket: socketIo.Socket, messages: IStorageMessage[]): void {
        socket.emit('user-conversation-messages', messages);
    }

    public emitFeedsToUser(socket: SocketIO.Socket, feeds: IFeed[]): void {
        socket.emit('feeds', feeds);
    }

    public emitNewFeedToUser(socket: socketIo.Socket, feed: IFeed): void {
        socket.emit('new-feed', feed);
    }

    public emitNewFeedLike(socket: socketIo.Socket, feedLike: IFeedLike): void {
        socket.emit('new-feed-like', feedLike);
    }

    public emitNewComment(socket: SocketIO.Socket, comment: IComment): void {
        socket.emit('new-comment', comment);
    }

    public emitCommentsFetched(socket: SocketIO.Socket, comments: IComment[]): void {
        socket.emit('comments-fetched', comments)
    }

    public emitNewStory(socket: SocketIO.Socket, story: IStory): void {
        socket.emit('new-story', story);
    }

    public emitStoriesFetched(socket: SocketIO.Socket, stories: IStory[]): void {
        socket.emit('stories-fetched', stories)
    }
}
