import {expect} from 'chai'
import UserRoomStorage, {IUserRoomStorage, Rooms} from '../UserRoomStorage';
let userRoomStorage: IUserRoomStorage;
describe('Integration tests for UserRoomStorage', () => {
    beforeEach(() => {
        userRoomStorage = new UserRoomStorage();
    });
    it('it should not remove if room is not found', () => {
        userRoomStorage.removeUserFromRoom('test', 'test1 ')
    });
    it('should not set in room if room is not found', () => {
        userRoomStorage.setUserInRoom('test', 'test');
        expect(userRoomStorage.getUsersFromRoom('test')).to.be.length(0)
    });
    it('should set user in storage for valid room', () => {
       userRoomStorage.setUserInRoom(Rooms.FeedScreen, '12345');
       expect(userRoomStorage.getUsersFromRoom(Rooms.FeedScreen)).to.be.length(1);
    });
    it('should remove user from room', () => {
        userRoomStorage.setUserInRoom(Rooms.FeedScreen, '12s345');
        expect(userRoomStorage.getUsersFromRoom(Rooms.FeedScreen)).to.be.length(1);
        userRoomStorage.removeUserFromRoom(Rooms.FeedScreen, '1s2345');
        expect(userRoomStorage.getUsersFromRoom(Rooms.FeedScreen)).to.be.length(0);
    });
});
