import * as socketIo from 'socket.io';
import {IUserSocketStorage, IUserSocketRepository} from '../models';
import config from '../../config/environment'

export default class UserSocketRepository implements IUserSocketRepository {
    private userSocketStorage: IUserSocketStorage;

    constructor(userSocketStorage: IUserSocketStorage) {
        this.userSocketStorage = userSocketStorage;
    }

    public isUserAlreadyConnected(socket: socketIo.Socket): boolean {
        if (typeof socket.handshake.query.userId !== 'undefined') {
            const localSocket = this.userSocketStorage.socketForUser(socket.handshake.query.userId);
            console.log(localSocket);
            if (localSocket === undefined) return false;
        }

        return false;
    }

    public userConnected(socket: socketIo.Socket): boolean {
        if (typeof socket.handshake.query.userId !== 'undefined') {
            this.userSocketStorage.setSocket(socket.handshake.query.userId, socket);
            return true;
        }
        return false;
    }

    public userDisconnected(socket: socketIo.Socket): void {
        if (typeof socket.handshake.query.userId !== 'undefined') {
            console.log('user disconected', socket.handshake.query.userId);
            this.userSocketStorage.removeUser(socket.handshake.query.userId)
        }
    }

    public socketForUserId(userId: string): socketIo.Socket | undefined {
        console.log('hit for user id', userId);
        return this.userSocketStorage.socketForUser(userId);
    }

    public isAdmin(socket: socketIo.Socket): boolean {
        if (typeof socket.handshake.query.adminKey === 'undefined') return false;

        return socket.handshake.query.adminKey === config.adminKey
    }

    public userIdFromSocket(socket: socketIo.Socket): string {
        if (typeof socket.handshake.query.userId !== 'undefined') {
            return socket.handshake.query.userId;
        }
        return '';
    }

    public getAllClients(): [] {
        return this.userSocketStorage.getAllClients();
    }
}
