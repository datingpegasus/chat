import * as socketIo from 'socket.io';
import {IUserSocketStorage} from '../models';

export default class UserSocketStorage implements IUserSocketStorage {
    private readonly clients: any;
    private readonly rooms: any;

    private static instance: UserSocketStorage;

    private constructor() {
        this.clients = [];
        this.rooms = [];
    }
    public static getInstance(): IUserSocketStorage {
        if (!UserSocketStorage.instance) {
            UserSocketStorage.instance = new UserSocketStorage();
        }
        return UserSocketStorage.instance;
    }
    public setSocket(userId: string, socket: socketIo.Socket): void {
        this.clients[userId] = socket;
    }

    public socketForUser(userId: string): socketIo.Socket | undefined {
        return this.clients[userId];
    }

    public removeUser(userId: string): boolean | undefined {
        return delete this.clients[userId];
    }

    public getAllClients(): [] {
        return this.clients;
    }
}
