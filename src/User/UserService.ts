import {IUserRoomStorage} from './UserRoomStorage';
import {IKafkaProducer} from '../models';
import Logger from '../services/Logger';

export interface IUserService {
    userChangedScreen(data: any): void;
    newImageUploaded(data: any): Promise<void>;
    newStoryAdded(data: any): Promise<void>;
}
export default class UserService implements IUserService {
    private readonly userRoomStorage: IUserRoomStorage;
    private readonly kafkaProducer: IKafkaProducer;
    constructor(userRoomStorage: IUserRoomStorage, kafkaProducer: IKafkaProducer) {
        this.userRoomStorage = userRoomStorage;
        this.kafkaProducer = kafkaProducer;
    }

    public userChangedScreen(data: any): void {
        console.log('hit for a room', data.room);
        this.userRoomStorage.setUserInRoom(data.room, data.userId);
    }

    public async newImageUploaded(data: any): Promise<void> {
        try {
            await this.kafkaProducer.dispatchNewImageUploaded(data);
        } catch (e) {
            Logger.error(e);
        }
    }

    public async newStoryAdded(data: any): Promise<void> {
        try {
            await this.kafkaProducer.dispatchNewStoryAdded(data);
        } catch (e) {
            Logger.error(e);
        }
    }
}
