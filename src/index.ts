import 'module-alias/register';
import SocketIo from './services/SocketIo';
import UserSocketRepository from './User/UserSocketRepository';
import UserSocketStorage from './User/UserSocketStorage';
import UserSocket from './socket/SocketService';
import Validator from './services/Validator';
import UserSocketEmit from './User/UserSocketEmit';
import KafkaProducer from './kafka/KafkaProducer';
import {kafka} from './modules/kafka';
import Redis from './services/Redis';
import config from '../config/environment';
import KafkaConsumer from './kafka/KafkaConsumer';
import Logger from './services/Logger';
import UserService from './User/UserService';
import UserRoomStorage from './User/UserRoomStorage';

export default function appSetup(server: any): Promise<null> {
    return new Promise<null>(async (resolve, reject) => {
        try {
            const producer = kafka.producer();
            await producer.connect();
            const kafkaProducer = new KafkaProducer(producer);
            const redisService = Redis.factory(config);
            await redisService.connectToRedis();
            const userRoomStorage = new UserRoomStorage();
            const userSocketRepository = new UserSocketRepository(UserSocketStorage.getInstance());
            const userSocket = new UserSocket(new Validator(), userSocketRepository, kafkaProducer,
                UserSocketEmit.factory(), redisService, userRoomStorage);
            const socket = new SocketIo(server, userSocketRepository, userSocket, new UserService(userRoomStorage, kafkaProducer));
            const connectedSocket = socket.connect();
            const consumer = kafka.consumer({groupId: config.kafka.groupId});
            await consumer.connect();
            const kafkaConsumer = new KafkaConsumer(consumer, userSocket, new Validator());
            await kafkaConsumer.subscribeToTopics();
            await consumer.run({
                autoCommit: false,
                eachMessage: async ({topic, partition, message}) => {
                    await kafkaConsumer.processKafkaTopic(topic, message, partition);
                }
            });
            resolve(null);
        } catch (e) {
            Logger.error(e);
            reject(false)
        }
    })
}
