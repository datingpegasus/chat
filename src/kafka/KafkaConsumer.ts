import * as kafka from 'kafkajs'
import {IValidator, IKafkaConsumer, RedisKeys} from '../models';
import SocketService from '../socket/SocketService';
import Redis from '../services/Redis';
import Logger from '../services/Logger';

export default class KafkaConsumer implements IKafkaConsumer {
    private kafkaConsumer: kafka.Consumer;
    private readonly validator: IValidator;
    private readonly socketService: SocketService;
    public static topics = {
        userMessageSaved: 'SavedMessage', deliveredMessageSaved: 'DeliveredMessageSaved',
        userStatusChange: 'UserStatusActivity', userAuthenticated: 'UserAuthenticated',
        userConversations: 'UserConversations', userConversationMessages: 'UserConversationMessages',
        feedsFetchingComplete: 'FeedsFetchingComplete', newFeedSaved: 'NewFeedSaved',
        newFeedLikeSaved: 'NewFeedLikeSaved', newCommentSaved: 'NewCommentSaved',
        commentsFetched: 'CommentsFetched', newStorySaved: 'NewStorySaved',
        storiesFetched: 'AllStoriesFetched'
    };

    constructor(kafkaConsumer: kafka.Consumer, socketService: SocketService, validator: IValidator) {
        this.kafkaConsumer = kafkaConsumer;
        this.validator = validator;
        this.socketService = socketService;
    }

    public async processKafkaTopic(topic: string, kafkaMessage: kafka.KafkaMessage, partition: number): Promise<void> {
        try {
            const kafkaParsedMessage = JSON.parse(kafkaMessage.value.toString());
            let saved = false;
            if (topic === KafkaConsumer.topics.deliveredMessageSaved) {
                saved = await this.socketService.emitConfirmMessageDelivered(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.userMessageSaved) {
                saved = await this.socketService.sendNewMessageToUser(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.userStatusChange) {
                saved = await this.socketService.userStatusChange();
            } else if (topic === KafkaConsumer.topics.userAuthenticated) {
                const redis = Redis.factory();
                saved = await redis.hSet(RedisKeys.onlineUsers, kafkaParsedMessage.id, JSON.stringify(kafkaParsedMessage));
            } else if (topic === KafkaConsumer.topics.userConversations) {
                saved = await this.socketService.sendUserConversationsToUser(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.userConversationMessages) {
                saved = await this.socketService.sendUserConversationMessages(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.feedsFetchingComplete) {
                saved = await this.socketService.feedsFetchingComplete(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.newFeedSaved) {
                saved = await this.socketService.newFeedSaved(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.newFeedLikeSaved) {
                saved = await this.socketService.newFeedLikeSaved(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.newCommentSaved) {
                saved = await this.socketService.newCommentSaved(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.commentsFetched) {
                saved = await this.socketService.commentsFetched(kafkaParsedMessage);
            } else if (topic === KafkaConsumer.topics.newStorySaved) {
                saved = await this.socketService.newStorySaved(kafkaParsedMessage);
            }else if (topic === KafkaConsumer.topics.storiesFetched) {
                saved = await this.socketService.storiesFetched(kafkaParsedMessage);
            } else {
                Logger.warn({kafka: `Wrong kafka topic name ${topic}`});
            }
            if (saved) {
                await this.commitOffset(topic, partition, kafkaMessage.offset);
            }
        } catch (e) {
            Logger.error(e);
        }

    }

    public async commitOffset(topicName: string, partition: number, offset: string): Promise<boolean> {
        try {
            await this.kafkaConsumer.commitOffsets([{
                topic: KafkaConsumer.topics.userConversationMessages,
                partition,
                offset
            }]);
            return true;
        } catch (e) {
            Logger.error(e);
            return false;
        }
    }

    public async subscribeToTopics(): Promise<void> {
        for (const [topicKey, topicValue] of Object.entries(KafkaConsumer.topics)) {
            await this.kafkaConsumer.subscribe({topic: topicValue})
        }
    }
}
