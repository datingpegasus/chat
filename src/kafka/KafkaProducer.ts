import {IMessage, IUserConnectedToConversation, IFeedBeforeSave, IFeedLike, IComment, ICommentBeforeSave} from 'types';
import {IKafkaProducer} from '../models'
import * as kafka from 'kafkajs';

export default class KafkaProducer implements IKafkaProducer {
    private producer: kafka.Producer;
    public static messageDelivered = 'MessageDelivered';
    public static newMessage = 'ProfanityCheck';
    public static userConnectedToSocket = 'UserConnectedToSocket';
    public static userConnectedToConversation = 'UserConnectedToConversation';
    public static userNotInConversation = 'UserNotInConversation';
    public static feeds = 'Feeds';
    public static newFeed = 'FeedProfanityCheck';
    public static newFeedLike = 'NewFeedLike';
    public static newComment = 'CommentProfanityCheck';
    public static newImage = 'NewImage';
    public static newStory = 'NewStory';
    public static fetchComments = 'FetchComments';
    constructor(producer: kafka.Producer) {
        this.producer = producer;
    }
    public async dispatchNewMessageDelivered(message: IMessage): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.messageDelivered,
            messages: [
                { value: JSON.stringify(message) }
            ]
        });
        return (sent[0].errorCode === 0);
    }
    public async dispatchNewMessage(message: IMessage): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.newMessage,
            messages: [
                { value: JSON.stringify(message) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async dispatchUserConnectedToSocket(userId: string): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.userConnectedToSocket,
            messages: [
                { value: JSON.stringify({ userId }) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async userConnectedToConversation(userConnectedToConversation: IUserConnectedToConversation): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.userConnectedToConversation,
            messages: [
                { value: JSON.stringify(userConnectedToConversation) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async dispatchUserNotInConversation(message: IMessage): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.userNotInConversation,
            messages: [
                { value: JSON.stringify(message) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async dispatchNewFeed(feed: IFeedBeforeSave) {
        const sent = await this.producer.send({
            topic: KafkaProducer.newFeed,
            messages: [
                { value: JSON.stringify(feed) }
            ]
        });
        return (sent[0].errorCode === 0);
    }
    public async dispatchNewFeedLike(feedLike: IFeedLike): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.newFeedLike,
            messages: [
                { value: JSON.stringify(feedLike) }
            ]
        });
        return (sent[0].errorCode === 0);
    }
    public async dispatchFetchFeeds(userId: string): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.feeds,
            messages: [
                { value: JSON.stringify({userId}) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async dispatchNewComment(comment: ICommentBeforeSave): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.newComment,
            messages: [
                { value: JSON.stringify(comment) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async dispatchNewImageUploaded(data: any): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.newImage,
            messages: [
                { value: JSON.stringify(data) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async dispatchNewStoryAdded(data: any): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.newStory,
            messages: [
                { value: JSON.stringify(data) }
            ]
        });
        return (sent[0].errorCode === 0);
    }

    public async dispatchCommentsForFeedId(feedId: number, userId: number): Promise<boolean> {
        const sent = await this.producer.send({
            topic: KafkaProducer.fetchComments,
            messages: [
                { value: JSON.stringify({feedId, userId})}
            ]
        });
        return (sent[0].errorCode === 0);
    }
}
