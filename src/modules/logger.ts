//@ts-ignore
import * as fluentLogger from 'fluent-logger/lib';
import config from '../../config/environment';

fluentLogger.configure(config.logger.serviceName, {
    host: config.logger.ipAddress,
    port: config.logger.port,
    timeout: 3.0,
    reconnectInterval: 600000 // 10 minutes
});

export default fluentLogger
