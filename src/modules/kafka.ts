import {Kafka, logLevel} from 'kafkajs';
import config from '@config/index';
import logger from './logger'
//@ts-ignore
const MyLogCreator = l => ({ namespace, level, label, log }) => {
    logger.emit(label, log);
};
export const kafka = new Kafka({
    clientId: config.kafka.clientId,
    brokers: config.kafka.brokers,
    logLevel: logLevel.ERROR,
    logCreator: MyLogCreator
});
